<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    protected function jwt(User $user) {
        $payload = [
            'iss' => "http://www.codemoon.com.br/lumenapi", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60, // Expiration time            
        ];
        
        return JWT::encode($payload, env('JWT_SECRET'));
    } 
    
    public function authenticate(Request $request) {
        
        $validator = Validator::make($request->only('email', 'password'), [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'Sintaxe Incorreta'
            ], 400);
        } 
                
        $user = User::where('email', $request->input('email'))->where('ativo', 1)->first();        
        if (!$user) {            
            return response()->json([
                'error' => 'Email ou Senha inválidos'
            ], 400);
        }
        
        if (Hash::check($request->input('password'), $user->password)) {
            return response()->json([       
                'data' => $user,         
                'token' => $this->jwt($user)
            ], 200);
        }
        // Bad Request response
        return response()->json([
            'error' => 'Email or password is wrong.'
        ], 400);
    }
}
